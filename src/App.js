import React, { Component } from 'react';
import './App.css';

import axios from 'axios';
import moment from 'moment';

import DarkSkyApi from 'dark-sky-api';
import Moment from 'react-moment';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Card, Lookup, ProgressCircular } from 'react-rainbow-components';
import ReactAnimatedWeather from 'react-animated-weather';

DarkSkyApi.apiKey = 'c8d48fc4b8b71e93251ca84a6d931ade';

const defaults = {
  icon: 'CLEAR_DAY',
  color: 'goldenrod',
  animate: true,
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: 20,
      loading: true,
      dateToFormat: null,
      today: {},
      forecast: {},
      options: [],
      option: null,
      value: '',
      position: {
        latitude: 37.8267,
        longitude: -122.4233,
      }
    };
  }

  componentDidMount() {
    this.setState({ progress: 40 }, () => {
      this.callApi();
      this.callApi2();
    });
  }

  componentWillUnmount() {
    clearInterval(this.startPolling)
  }

  startPolling = () => {
    clearInterval(this.startPolling)
    setInterval(() => {
      let now = new moment();
      if (moment(this.state.dateToFormat).isBefore(now)) {
        this.callApi();
        this.callApi2();
      }
    }, 150000);
  }

  convertedTemp = (value) => {
    return ((value * 9/5) + 32)
  }

  callApi = async position => {
    this.setState({ progress: 50 });
    await DarkSkyApi.loadCurrent(position ? position : null).then(result => {
      console.log(result);
      let today = {
        result,
        ...this.state.today
      }
      this.setState({ progress: 70, dateToFormat: result.dateTime, currentIcon: result.icon, today });
    });
  };

  callApi2 = async position => {
    await DarkSkyApi.loadForecast(position ? position : null).then(result => {
      console.log(result);
      this.setState({ progress: 100, loading: false, forecast: result.daily.data.slice(0, 5) }, () => {
        this.startPolling();
      });
    });
  };

  getCoords = async value => {
    // https://opencagedata.com/api#forward-resp
    if (value.length) {
      const req = `https://api.opencagedata.com/geocode/v1/json?q=${value}&key=8c8892c16d9b4780a150d7369266bafe`;
      let options = [];
      this.setState({ value });

      axios.get(req).then(res => {
        // console.log(res.data.results);

        for (let i = 0; i < res.data.results.length; i++) {
          const result = res.data.results[i];

          let option = {
            label: result.formatted,
            geometry: {
              latitude: result.geometry.lat,
              longitude: result.geometry.lng,
            },
          };

          options.push(option);
        }

        this.setState({ options, value });
      });
    } else {
      this.setState({
        option: null,
        options: null,
      });
    }
  };

  setCoords = ({ option }) => {
    if (option) {
      // console.log(option);
      let position = {
        latitude: option.geometry.latitude,
        longitude: option.geometry.longitude,
      };
      this.setState({ position, option }, async () => {
        await this.callApi(this.state.position);
        await this.callApi2(this.state.position);
      });
    } else {
      this.setState({
        option: null,
        options: null,
      });
    }
  };

  render() {
    const { currentIcon, dateToFormat, forecast, option, options, progress, today } = this.state;

    return (
      <div className="App">
        <Grid fluid>
          <Row>
            {this.state.loading ? (
              <Col xs={12}>
                <Row center="xs">
                  <Col xs={4} style={{ marginTop: '10%' }}>
                    <ProgressCircular value={progress} variant="warning" style={{ margin: 'auto' }} />
                    <h1 className="rainbow-font-size-heading_small rainbow-color_gray-3">Fetching Your Forecast...</h1>
                  </Col>
                </Row>
              </Col>
            ) : (
              <Col xs={12}>
                <h1 style={{fontSize: 64}}>Hello, weather!</h1>
                <Row>
                  <Col md={12}>
                    <Lookup
                      id="lookup-1"
                      placeholder="Search for your city..."
                      options={options}
                      value={option}
                      onChange={option => this.setCoords({ option })}
                      onSearch={this.getCoords}
                      debounce
                      size={'large'}
                      className="rainbow-m-vertical_x-large rainbow-p-horizontal_medium rainbow-m_auto"
                      style={{ width: '100%' }}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={2} style={{ padding: 15 }}>
                    <Card>
                      <ReactAnimatedWeather
                        icon={
                          currentIcon
                            ? currentIcon
                                .split('-')
                                .join('_')
                                .toUpperCase()
                            : ''
                        }
                        color={defaults.color}
                        size={defaults.size}
                        animate={defaults.animate}
                      />
                      {today ? <p>
                        <strong>Summary:</strong> {today.summary}
                        <hr/>
                        <h2>Temp</h2>
                        Current temp: {this.convertedTemp(today.temperature)}
                        Feels Like: {this.convertedTemp(today.apparentTemperature)}
                        <hr/>
                        <h2>Atmosphere</h2>
                        Humidity: {today.humidity}
                        UV Index: {today.uvIndex}
                        Pressure: {today.pressure}
                        <hr/>
                        <h2>Wind</h2>
                        Speed: {today.windSpeed}
                        Gust: {today.windGust}
                        Direction: {today.windDirection}
                        Bearing: {today.windBearing}
                      </p>:null}
                    </Card>
                  </Col>
                  <Col md={10} style={{ textAlign: 'left' }}>
                    <p>
                      <strong>Last Updated:</strong> <Moment date={dateToFormat} format="h:mm A (Z) - dddd, MMM. Do, YYYY" />
                    </p>
                  </Col>
                </Row>
                <hr />
                <h2>5 Day Forecast</h2>
                <Row>
                  <Col xs={12}>
                    <Row>
                      {forecast.map((day, index) => {
                        return (
                          <Col md={2} key={index}>
                            <Card>
                              <ReactAnimatedWeather
                                icon={
                                  day.icon
                                    ? day.icon
                                        .split('-')
                                        .join('_')
                                        .toUpperCase()
                                    : ''
                                }
                                color={defaults.color}
                                size={defaults.size}
                                animate={defaults.animate}
                              />
                              <p>
                                <strong>Summary:</strong> {day.summary}
                              </p>
                            </Card>
                          </Col>
                        );
                      })}
                    </Row>
                  </Col>
                </Row>
              </Col>
            )}
          </Row>
        </Grid>
      </div>
    );
  }
}

export default App;
